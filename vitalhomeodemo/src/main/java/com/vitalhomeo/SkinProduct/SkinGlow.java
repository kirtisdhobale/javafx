package com.vitalhomeo.SkinProduct;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;


public class SkinGlow extends Application {

    @Override
    public void start(Stage prStage) throws Exception {
    
        prStage.setTitle("VitalHomeo");
        prStage.setHeight(700);
        prStage.setWidth(800); 
        prStage.centerOnScreen();

        Label label1 = new Label("Recommonded For SkinGlow..");
        label1.setBackground(new Background(new BackgroundFill(Color.CADETBLUE, CornerRadii.EMPTY, null)));
        label1.setPadding(new javafx.geometry.Insets(10));
        label1.setStyle("-fx-font-size:25;-fx-font-weight:bold");
        label1.setLayoutX(600);
        label1.setLayoutY(30);
        

        Image ig1=new Image("Skinglow\\glow1.png");
        ImageView iv1 =new ImageView(ig1);
        iv1.setFitWidth(500);
        iv1.setFitHeight(150);
        iv1.setPreserveRatio(true);
        iv1.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Hahnemann Glow More For Acne & Scar Drop ");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popuplabel2=new Label("Brand:        \t\t\tHahnemann\nItem Form:        \t\tDrop\nBenefits:     \t\t\tGlow Skin\nMaterial Type:\t\t\tNatural\nSkin Type:\t\t\tAll \nSpecial Feature:\t\tTravel Size\nNet Quantity:\t\t\t30 ml Drop\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popuplabel2);

               Image ig1=new Image("Skinglow\\glow1.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Image ig2=new Image("Skinglow\\glow1.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox,click3);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("Skinglow\\glow1.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Hahnemann Glow More For Acne & Scar Drop");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹138");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹124");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("10% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹138");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹14");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹124");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb1 = new Label("   Glow More For Acne \n   & Scar drop $124");
        //lb1.setFont(new Font(20));
        lb1.setStyle("-fx-font-size:17;-fx-font-weight:bold");
        lb1.setAlignment(Pos.CENTER);

        Label l1 = new Label("   Hahnemann Glow More For\t\n   Acne & Scar Drop");
        l1.setFont(new Font(15));
        
        //here starts code of image2

        Image ig2=new Image("Skinglow\\glow2.png");
        ImageView iv2 =new ImageView(ig2);
        iv2.setFitWidth(600);
        iv2.setFitHeight(150);
        iv2.setPreserveRatio(true);
        iv2.setOnMouseClicked(new EventHandler<MouseEvent>() {//annonymous class of image 2

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Adven Glow Aid Drops (30ml)");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("It is used for curing acne, scars, pimples, blemishes, and dark circles around eyes");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tAdven\nItem Form:        \t\tliqid(Drop)\nBenefits:     \t\t\tGlow Skin\nMaterial Type:\t\t\tChemical Free\nSkin Type:\t\t\tAll \nSpecial Feature:\t\tTravel Size\nIncluded Components:Drop with Homeo Components\nNet Quantity:\t\t\t30 ml\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("Skinglow\\glow2.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("Skinglow\\glow2-2.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox,click3);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("Skinglow\\glow2.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Adven Glow Aid Drops (30ml)");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹125");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹125");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("0% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹125");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹0");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹125");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                Button click3= new Button("Back");
                click3.setLayoutX(10);
                click3.setLayoutY(10);
                click3.setOnAction(e -> {
                        cartStage.close();
                });

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox,click3);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb2 = new Label("  Adven Glow Aid Drops \n\t(30ml)");//label of image 2
        //lb2.setFont(new Font(20));
        lb2.setStyle("-fx-font-size:17;-fx-font-weight:bold");
        Label l2 = new Label("It is used for curing acne,\nscars, pimples, blemishes,\nand dark circles around eyes");
        l2.setFont(new Font(15));

        //code of image 3

        Image ig3=new Image("Skinglow\\glow3.png");
        ImageView iv3 =new ImageView(ig3);
        iv3.setFitWidth(500);
        iv3.setFitHeight(150);
        iv3.setPreserveRatio(true);
        iv3.setOnMouseClicked(new EventHandler<MouseEvent>() {//annonymous function of image 3

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("SBL Glowing Beauty Fairness Cream (50g)\r\n");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("Lightens Skin Tone, Reduce Blemishes and Dark Spots, Acne and Pimples");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tSBL\nItem Form:        \t\tCream\nBenefits:     \t\tGlow Skin\nMaterial Type:\t\t\tChemical Free(Homeo)\nSkin Type:\t\t\tAll \nSpecial Feature:\t\tReducing Tan\nIncluded Components:Homeo Products\nNet Quantity:\t\t\t50 gm\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("Skinglow\\glow3.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("Skinglow\\glow3-2.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox,click3);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("Skinglow\\glow3.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("SBL Glowing Beauty Fairness Cream (50g)");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹100");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹98");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("2% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹100");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹2");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹98");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                Button click= new Button("Back");
                click.setLayoutX(10);
                click.setLayoutY(10);
                click.setOnAction(e -> {
                    cartStage.close();
               });


                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox,click);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb3= new Label("\tSBL Glowing Beauty \n\tFairness Cream");//label of image 3
        //lb1.setFont(new Font(20));
        lb3.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l3= new Label("\tLightens Skin Tone, Reduce \nBlemishes and Dark Acne and Pimples");//label of image3 inside
        l3.setFont(new Font(15));

        //here is the start of image 4 code

        Image ig4=new Image("Skinglow\\glow4.png");
        ImageView iv4 =new ImageView(ig4);
        iv4.setFitWidth(500);
        iv4.setFitHeight(150);
        iv4.setPreserveRatio(true);
        iv4.setOnMouseClicked(new EventHandler<MouseEvent>() {//annonymous function of image 4
            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Bakson Sunny Herbals Glamour Cream (For Men) (100g)");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("Provides nourishment and glow to your skin, Healthy, Moisturized and Beautiful Skin");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tSunny Herbel\nItem Form:        \t\tFace Creame\nBenefits:     \t\t\tpores/blackheads/whiteheads\nMaterial Type:\t\t\tChemical Free\nSkin Type:\t\t\tAll \nSpecial Feature:\t\tTravel Size\nIncluded Components:  Skin Treaitment Mask\nNet Quantity:\t\t\t100 grm\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("Skinglow\\glow4.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);

               VBox vBox1= new VBox(15,vb,hBox,click3);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,iv2,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("Skinglow\\glow4.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Bakson Sunny Herbals Glamour Cream (For Men) (100g)");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹160");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹150");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("6% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity:1");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹160");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹10");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹150");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                
               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                  cartStage.close();
               });

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox,click3);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb4 = new Label("Bakson Sunny Herbals \nGlamour Cream ");//label of image 4
        //lb1.setFont(new Font(20));
        lb4.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l4 = new Label("Provides nourishment and glow \nto your skin, Healthy, Moisturized \nand Beautiful Skin");//label of image 4 inside
        l4.setFont(new Font(15));

        //here is the start of image 5 code

        Image ig5=new Image("Skinglow\\glow5.png");
        ImageView iv5 =new ImageView(ig5);
        iv5.setFitWidth(500);
        iv5.setFitHeight(150);
        iv5.setPreserveRatio(true);
        iv5.setOnMouseClicked(new EventHandler<MouseEvent>() {//annonymous function of image 5

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Dr Batra's Pro+ Skin Clear Serum for Acne-Free Skin ");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label(" with Salicylic Acid & Niacinamide - 50ml");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tDr Batra's\nItem Form:        \t\tLiquid\nBenefits:     \t\t\tExfoliating\nSkin Tone:  \t\t\t\tAll\nMaterial Type:\t\t\tNatural\nSpecial Feature:\t\tTravel Size\nIncluded Components:  Skin Treaitment Mask\nNet Quantity:\t\t\t50.0 millilitre\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("Skinglow\\glow5-2.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("Skinglow\\glow5.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });

               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox,click3);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("Skinglow\\glow5.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Dr Batra's Pro+ Skin Clear Serum for Acne-Free Skin");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹999");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹599");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("40% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹999");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹400");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹599");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                Button backButton= new Button("Back");
                backButton.setLayoutX(750);
                backButton.setLayoutY(10);
                backButton.setOnAction(e -> {
                     cartStage.close();
                });


                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox,backButton);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb5 = new Label("Dr Batra's Pro+ Skin Clear\nSerum for Acne-Free Skin");//label of image 5
        //lb1.setFont(new Font(20));
        lb5.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l5 = new Label("with Salicylic Acid &\nNiacinamide - 50ml");//label of image 5 inside
        l5.setFont(new Font(15));

        //here is the start of code of image 6


        Image ig6=new Image("Skinglow\\glow6.png");
        ImageView iv6 =new ImageView(ig6);
        iv6.setFitWidth(500);
        iv6.setFitHeight(150);
        iv6.setPreserveRatio(true);
        iv6.setOnMouseClicked(new EventHandler<MouseEvent>() {//annonymous function of image 6

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Herbal Green Adven Naturals Face Wash, Gel, Packaging Size: 200ml\n₹150");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel2 = new Label("Brand:\t\t\t\tAdven Natural\nUsage/Application:\t\tFace Wash\nForm:\t\t\t\tGel\nColor:\t\t\t\tGreen\nPackaging Size:\t\t200ml\nIngredient:\t\t\tHerbal\nGender:\t\t\t\tUnisex");
               popupLabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel2);

               Image ig2=new Image("Skinglow\\glow6.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("Skinglow\\glow6-2.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox,click3);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 1000, 800);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("Skinglow\\glow6.png"));
                productImage.setFitWidth(150);
                productImage.setFitHeight(150);

                // Product name
                Label productName = new Label("Herbal Green Adven Naturals Face Wash and Gel");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                //backbutton
                Button backButton= new Button("Back");
                backButton.setLayoutX(500);
                backButton.setLayoutY(10);
                backButton.setOnAction(e -> {
                     cartStage.close();
                });


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹200");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹150");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("15% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹200");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹50");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹150");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox,backButton);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb6 = new Label("Herbal Green Adven \nNaturals Face Wash ₹150");//label of image 6
        //lb1.setFont(new Font(20));
        lb6.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l6 = new Label("Mamaearth Tea Tree Spot Gel\nFace Cream with Tea Tree");//label of image 6 inside
        l6.setFont(new Font(15));

        //here is the start of code of image 7

        Image ig7=new Image("Skinglow\\glow7.png");
        ImageView iv7 =new ImageView(ig7);
        iv7.setFitWidth(500);
        iv7.setFitHeight(150);
        iv7.setPreserveRatio(true);
        iv7.setOnMouseClicked(new EventHandler<MouseEvent>() {//annonymous function of image 7

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("\t\t\t\t\t\t\t\t\t\t\tKey Ingredients ");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel2 = new Label("\t\t\t\t\t\t\t\t\t\t\tAyurveda texts and modern research back the following facts");
               popupLabel2.setStyle("-fx-font-size:18;");

               VBox vb=new VBox(15,popupLabel,popupLabel2);
               vb.setLayoutX(250);

               Image ig1=new Image("Skinglow\\himalaya2.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(2000);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
              // iv1.setLayoutX(350);
               //iv1.setLayoutY(400);

               Image ig2=new Image("Skinglow\\kesar.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(400);
               iv2.setFitHeight(150);
               iv2.setPreserveRatio(true);
               //iv2.setLayoutX(250);
               //iv2.setLayoutY(400);

               Image ig3=new Image("Skinglow\\mint.png");
               ImageView iv3 =new ImageView(ig3);
               iv3.setFitWidth(400);
               iv3.setFitHeight(150);
               iv3.setPreserveRatio(true);
               //iv3.setLayoutX(250);
               //iv3.setLayoutY(400);

               Image ig4=new Image("Skinglow\\Pomegranate.png");
               ImageView iv4 =new ImageView(ig4);
               iv4.setFitWidth(400);
               iv4.setFitHeight(150);
               iv4.setPreserveRatio(true);
               //iv4.setLayoutX(250);
               //iv4.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(200, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(200, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button backButton= new Button("Back");
              // backButton.setLayoutX(10);
               //backButton.setLayoutY(10);
               backButton.setOnAction(e -> {
                    popupStage.close();
               });

               Label lb1 = new Label("Kesar");
               lb1.setStyle("-fx-font-size:25;-fx-font-weight:bold");
               Label lb2 = new Label("Mint");
               lb2.setStyle("-fx-font-size:25;-fx-font-weight:bold");
               Label lb3 = new Label("Pomegranate");
               lb3.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label l1 = new Label("Kesar is a luxurious herb that has been used in skincare for\ncenturies for a fairer, brighter complexion.");
               Label l2 = new Label("Mint is an antiseptic and an excellent skin cleanser.It helps\nsoothe the skin.");
               Label l3 = new Label("Pomegranate is a rich source of polyphenols. It is reported \nto possess potent antioxidant and protective benefits\nagainst free radicals generated by the environment.\nPomegranate help softens and nourishes the skin.");
               Label label=new Label("Pack Of –\t1\r\nQuantity – 100 ml\r\nContainer Type – Tube\r\nProduct Type – Face wash\r\nUsed For - It helps to get reveal the brighten & glowing skin\r\nSkin Type – All skin type\r\nIdeal For -   Men & Women");
               label.setStyle("-fx-font-size:18");

               Label label1=new Label("                     ");
               Label label2=new Label("               ");
               Label label3=new Label("                ");

               HBox hBox = new HBox(15,addToCartButton,buyNowButton,backButton);

               VBox vBox2= new VBox(15,label,hBox);
               vBox2.setLayoutX(80);

               HBox hBox2 = new HBox(180,iv1,vBox2);
               hBox2.setLayoutX(250);
               hBox2.setLayoutY(250);
               hBox2.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
               hBox2.setPadding(new Insets(10));

       


               HBox hBox3 = new HBox(120,label1,iv2,iv3,iv4);
               hBox3.setLayoutX(50);
               hBox3.setLayoutY(250);

               HBox hBox4 = new HBox(230,label2,lb1,lb2,lb3);
               hBox4.setLayoutX(50);
               hBox4.setLayoutY(250);

               HBox hBox5 = new HBox(50,label3,l1,l2,l3);
               hBox5.setLayoutX(50);
               hBox5.setLayoutY(450);


               VBox vBox1= new VBox(15,hBox2,vb,hBox3,hBox4,hBox5);
               vBox1.setLayoutX(80);
       

               popupLayout.getChildren().add(vBox1);
               Scene popupScene = new Scene(popupLayout, 800, 600);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("Skinglow\\himalaya2.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Natural Glow Kesar Face Wash");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹180");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹162");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("5% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹180");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹18");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹162");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                Button backButton= new Button("Back");
                backButton.setLayoutX(750);
                backButton.setLayoutY(10);
                backButton.setOnAction(e -> {
                     cartStage.close();
                });

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox,backButton);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb7 = new Label("Natural Glow Kesar Face Wash");//label of image 7
        //lb1.setFont(new Font(20));
        lb7.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l7 = new Label("\nBrightens and reveals glowing skin");//label of image 7 inside
        l7.setFont(new Font(15));

        //here is the start of image 8

        Image ig8=new Image("blackheads\\blackheads8.png");
        ImageView iv8 =new ImageView(ig8);
        iv8.setFitWidth(600);
        iv8.setFitHeight(150);
        iv8.setPreserveRatio(true);
        iv8.setOnMouseClicked(new EventHandler<MouseEvent>() {//annonymous function of image 8

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Dr Batra'S Instant Glow Face Wash");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel2 = new Label("Enriched With Tumeric For Healthy & Glowing Skin - 200 Gm");
               popupLabel2.setStyle("-fx-font-size:18;");

               Label popuplabel3=new Label("Dr Batra’s Instant Glow Face Wash is infused with the goodness of Turmeric and Echinacea extracts. These time-tested ingredients work together in synergy to moisturize and enhance overall skin health. This face wash for glowing skin helps to eliminate daily impurities and the effects of pollutants, leaving your skin feeling smooth and looking radiant. Free from harsh chemicals, this face wash is an ideal choice for glowing skin.\r\n" + //
                                      "\r\n" + //
                                      "Ingrediants: Purified Water, Acrylate Copolymer, Sodiumlauryl Sarcosinate, Capb, Sodium Cocoylisothianates, Glycerione, Alovera, Curcuma Longa Extract, Niacinamide , Vit E Beads, Echinacea Extract, Methylchloroisothiazolinone And Methylisothiazolinone, Sodium Hydroxide, Disodium Edta, Glycolic Acid, Pefume Peg-40 Hydrogenated Castor Oil, Ci 19140, 14720, Citric Acid.\r\n" + //
                                      "\r\n" + //
                                      "Usage: Rinse Face With Lukewarm Water. Apply Dr Batra’S Instant Glow Face Wash On Face And Work Up A Lather. Massage On Face And Neck In A Circular Motion. Wash Off And Pat Dry.\r\n" + //
                                      "\r\n" + //
                                      "Disclaimer: For external use only. Avoid contact with eyes. Keep out of the reach of children. Dr Batra’s products are approved by the Food and Drug Administration and are animal cruelty-free.");
               popuplabel3.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel2,popuplabel3);

               Image ig2=new Image("blackheads\\blackheads8.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheads8.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button backButton= new Button("Back");
               backButton.setLayoutX(10);
               backButton.setLayoutY(10);
               backButton.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox,backButton);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 800, 600);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("Skinglow\\glow8.png"));
                productImage.setFitWidth(400);
                productImage.setFitHeight(200);

                // Product name
                Label productName = new Label("Dr Batra'S Instant Glow Face Wash");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹249");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹199");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("20% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                Button backButton= new Button("Back");
                backButton.setLayoutX(500);
                backButton.setLayoutY(10);
                backButton.setOnAction(e -> {
                     cartStage.close();
                });

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹249");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹50");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹199");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox,backButton);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb8 = new Label("  Dr Batra'S Instant Glow \n\tFace Wash $199");//label of image 8
        //lb1.setFont(new Font(20));
        lb8.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l8 = new Label("Enriched With Tumeric For Heal-\nthy & Glowing Skin -200 Gm");//label of image 8 inside
        l8.setFont(new Font(15));

        //BackButton

        Button BackButton= new Button("Back");
        BackButton.setLayoutX(10);
        BackButton.setLayoutY(10);
        BackButton.setOnAction(e -> {
            System.out.println("Button clicked!");
        });

        VBox vb1 = new VBox(iv1,lb1,l1);
        vb1.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb1.setPadding(new Insets(10));

        VBox vb2 = new VBox(iv2,lb2,l2);
        vb2.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb2.setPadding(new Insets(10));

        VBox vb3 = new VBox(iv3,lb3,l3);
        vb3.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb3.setPadding(new Insets(10));

        VBox vb4 = new VBox(iv4,lb4,l4);
        vb4.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb4.setPadding(new Insets(10));

        VBox vb5 = new VBox(iv5,lb5,l5);
        vb5.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb5.setPadding(new Insets(10));

        VBox vb6 = new VBox(iv6,lb6,l6);
        vb6.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb6.setPadding(new Insets(10));

        VBox vb7 = new VBox(iv7,lb7,l7);
        vb7.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb7.setPadding(new Insets(10));

        VBox vb8 = new VBox(iv8,lb8,l8);
        vb8.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb8.setPadding(new Insets(10));


        HBox hb1=new HBox(50,vb1,vb2,vb3,vb4);
        hb1.setLayoutX(200);
        hb1.setLayoutY(100);

        HBox hb2=new HBox(50,vb5,vb6,vb7,vb8);
        hb2.setLayoutX(200);
        hb2.setLayoutY(400);

        Group gr = new Group(BackButton,label1,hb1,hb2);
        Scene  sc = new Scene(gr);
        prStage.setScene(sc);
        sc.setFill(Color.BISQUE);
        prStage.show();
    }
}