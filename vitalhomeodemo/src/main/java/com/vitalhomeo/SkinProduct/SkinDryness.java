package com.vitalhomeo.SkinProduct;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class SkinDryness extends Application{
    
    @Override
    public void start(Stage prStage) throws Exception {
    
        prStage.setTitle("VitalHomeo");
        prStage.setHeight(700);
        prStage.setWidth(800); 
        prStage.centerOnScreen();

        Label label1 = new Label("Recommonded For SkinDryness..");
        label1.setBackground(new Background(new BackgroundFill(Color.CADETBLUE, CornerRadii.EMPTY, null)));
        label1.setPadding(new javafx.geometry.Insets(10));
        label1.setStyle("-fx-font-size:25;-fx-font-weight:bold");
        label1.setLayoutX(53784e30);
        label1.setLayoutY(30);
        

        Image ig1=new Image("skindrynessimages\\skin1.png");
        ImageView iv1 =new ImageView(ig1);
        iv1.setFitWidth(500);
        iv1.setFitHeight(150);
        iv1.setPreserveRatio(true);
        iv1.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Sanfe Deep Purifying Nose Strips for Women & Men ");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("Homeo Essentials Xiro Derma Dry Skin Cream 50g | \nMoisturizer for dry, flaky skin | Chemical-free & Natural");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tHomeo Essentials\nItem Form:        \t\t50 Millilitres\nAge Range:      \t\t\tMaterial Type:\t\t\tChemical Free\nSkin Type:\t\t\tDry\nIncluded Components:  Jojoba,vitamin\nNet Quantity:\t\t\t50.0 gram\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("skindrynessimages\\skin1-2.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("skindrynessimages\\skin1.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2,click3);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheadsinfo1.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Sanfe Deep Purifying Nose Strips for Women & Men");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹600");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹495");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹600");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹105");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹495");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb1 = new Label("\tRemoval of Acne\n\t\t1400");
        //lb1.setFont(new Font(20));
        lb1.setStyle("-fx-font-size:17;-fx-font-weight:bold");
        lb1.setAlignment(Pos.CENTER);

        Label l1 = new Label("Dr Batra's PRO+ Hair Fall Control\nShampoo, AntiDandruff Sulphate");
        l1.setFont(new Font(15));
        
        //image2

        Image ig2=new Image("skindrynessimages\\skin2.png");
        ImageView iv2 =new ImageView(ig2);
        iv2.setFitWidth(500);
        iv2.setFitHeight(150);
        iv2.setPreserveRatio(true);
        iv2.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Sanfe Deep Purifying Nose Strips for Women & Men ");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("With Fuji Green Tea & Witch Hazel extracts | Removes Whiteheads | Blackheads \nand cleanses pores (Pack of 6) |Instant Result");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tSanfe\nItem Form:        \t\tPen Product\nBenefits:     \t\t\tpores/blackheads/whiteheads\nScent:  \t\t\t\tGreen Tea \nMaterial Type:\t\t\tFree Chemical Free\nSkin Type:\t\t\tAll \nSpecial Feature:\t\tTravel Size\nIncluded Components:  Skin Treaitment Mask\nNet Quantity:\t\t\t1.00 count\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("blackheads\\blackheads1.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheadsinfo1.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2,click3);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheadsinfo1.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Sanfe Deep Purifying Nose Strips for Women & Men");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹600");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹495");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹600");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹105");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹495");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb2 = new Label("      ACNE AID TABLETS\n\t\t1550");
        //lb2.setFont(new Font(20));
        lb2.setStyle("-fx-font-size:17;-fx-font-weight:bold");
        Label l2 = new Label("\tHelps To Remove Acne");
        l2.setFont(new Font(15));

        Image ig3=new Image("skindrynessimages\\skin3.png");
        ImageView iv3 =new ImageView(ig3);
        iv3.setFitWidth(500);
        iv3.setFitHeight(150);
        iv3.setPreserveRatio(true);
        iv3.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Sanfe Deep Purifying Nose Strips for Women & Men ");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("With Fuji Green Tea & Witch Hazel extracts | Removes Whiteheads | Blackheads \nand cleanses pores (Pack of 6) |Instant Result");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tSanfe\nItem Form:        \t\tPen Product\nBenefits:     \t\t\tpores/blackheads/whiteheads\nScent:  \t\t\t\tGreen Tea \nMaterial Type:\t\t\tFree Chemical Free\nSkin Type:\t\t\tAll \nSpecial Feature:\t\tTravel Size\nIncluded Components:  Skin Treaitment Mask\nNet Quantity:\t\t\t1.00 count\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("blackheads\\blackheads1.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheadsinfo1.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2,click3);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheadsinfo1.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Sanfe Deep Purifying Nose Strips for Women & Men");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹600");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹495");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹600");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹105");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹495");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb3 = new Label("  Himalaya Purifying Neem  \n\t\tFacewash\n\t\t  500");
        lb3.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l3 = new Label(" Helps to Brighten the skin 100 ml");
        l3.setFont(new Font(15));

        Image ig4=new Image("skindrynessimages\\skin4.png");
        ImageView iv4 =new ImageView(ig4);
        iv4.setFitWidth(600);
        iv4.setFitHeight(150);
        iv4.setPreserveRatio(true);
        iv4.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Sanfe Deep Purifying Nose Strips for Women & Men ");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("With Fuji Green Tea & Witch Hazel extracts | Removes Whiteheads | Blackheads \nand cleanses pores (Pack of 6) |Instant Result");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tSanfe\nItem Form:        \t\tPen Product\nBenefits:     \t\t\tpores/blackheads/whiteheads\nScent:  \t\t\t\tGreen Tea \nMaterial Type:\t\t\tFree Chemical Free\nSkin Type:\t\t\tAll \nSpecial Feature:\t\tTravel Size\nIncluded Components:  Skin Treaitment Mask\nNet Quantity:\t\t\t1.00 count\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("blackheads\\blackheads1.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheadsinfo1.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2,click3);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheadsinfo1.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Sanfe Deep Purifying Nose Strips for Women & Men");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹600");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹495");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹600");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹105");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹495");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb4 = new Label("\t  CeraVe 500ml\n\t\t1500");
        //lb1.setFont(new Font(20));
        lb4.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l4 = new Label("Hair care which conditions your hair");
        l4.setFont(new Font(15));

        Image ig5=new Image("skindrynessimages\\skin5.png");
        ImageView iv5 =new ImageView(ig5);
        iv5.setFitWidth(500);
        iv5.setFitHeight(150);
        iv5.setPreserveRatio(true);
        iv5.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Sanfe Deep Purifying Nose Strips for Women & Men ");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("With Fuji Green Tea & Witch Hazel extracts | Removes Whiteheads | Blackheads \nand cleanses pores (Pack of 6) |Instant Result");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tSanfe\nItem Form:        \t\tPen Product\nBenefits:     \t\t\tpores/blackheads/whiteheads\nScent:  \t\t\t\tGreen Tea \nMaterial Type:\t\t\tFree Chemical Free\nSkin Type:\t\t\tAll \nSpecial Feature:\t\tTravel Size\nIncluded Components:  Skin Treaitment Mask\nNet Quantity:\t\t\t1.00 count\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("blackheads\\blackheads1.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheadsinfo1.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2,click3);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheadsinfo1.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Sanfe Deep Purifying Nose Strips for Women & Men");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹600");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹495");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹600");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹105");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹495");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb5 = new Label("   Mamaearth TeaTree Gel   \n\t\t$500");
        //lb1.setFont(new Font(20));
        lb5.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l5 = new Label("Mamaearth Tea Tree Spot Gel\nFace Cream with Tea Tree");
        l5.setFont(new Font(15));

        Image ig6=new Image("skindrynessimages\\skin6.png");
        ImageView iv6 =new ImageView(ig6);
        iv6.setFitWidth(500);
        iv6.setFitHeight(150);
        iv6.setPreserveRatio(true);
        iv6.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Sanfe Deep Purifying Nose Strips for Women & Men ");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("With Fuji Green Tea & Witch Hazel extracts | Removes Whiteheads | Blackheads \nand cleanses pores (Pack of 6) |Instant Result");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tSanfe\nItem Form:        \t\tPen Product\nBenefits:     \t\t\tpores/blackheads/whiteheads\nScent:  \t\t\t\tGreen Tea \nMaterial Type:\t\t\tFree Chemical Free\nSkin Type:\t\t\tAll \nSpecial Feature:\t\tTravel Size\nIncluded Components:  Skin Treaitment Mask\nNet Quantity:\t\t\t1.00 count\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("blackheads\\blackheads1.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheadsinfo1.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2,click3);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheadsinfo1.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Sanfe Deep Purifying Nose Strips for Women & Men");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹600");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹495");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹600");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹105");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹495");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb6 = new Label("\tComedonin\n\t\t$500");
        //lb1.setFont(new Font(20));
        lb6.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l6 = new Label("Dr. Reckeweg R53 Acne Vulgaris\n& Pimples Drop Homeopathic\nMedicine For Skin");
        l6.setFont(new Font(15));

        Image ig7=new Image("skindrynessimages\\skin7.png");
        ImageView iv7 =new ImageView(ig7);
        iv7.setFitWidth(500);
        iv7.setFitHeight(150);
        iv7.setPreserveRatio(true);
        iv7.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Sanfe Deep Purifying Nose Strips for Women & Men ");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("With Fuji Green Tea & Witch Hazel extracts | Removes Whiteheads | Blackheads \nand cleanses pores (Pack of 6) |Instant Result");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tSanfe\nItem Form:        \t\tPen Product\nBenefits:     \t\t\tpores/blackheads/whiteheads\nScent:  \t\t\t\tGreen Tea \nMaterial Type:\t\t\tFree Chemical Free\nSkin Type:\t\t\tAll \nSpecial Feature:\t\tTravel Size\nIncluded Components:  Skin Treaitment Mask\nNet Quantity:\t\t\t1.00 count\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("blackheads\\blackheads1.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheadsinfo1.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2,click3);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheadsinfo1.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Sanfe Deep Purifying Nose Strips for Women & Men");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹600");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹495");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹600");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹105");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹495");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb7 = new Label("DERMATOUCH Bye Acne\n\t\t 500");
        //lb1.setFont(new Font(20));
        lb7.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l7 = new Label("DERMATOUCH Bye Bye Acne\nScars & Marks Cream Formulated\nSpecially");
        l7.setFont(new Font(15));

        Image ig8=new Image("skindrynessimages\\skin8.png");
        ImageView iv8 =new ImageView(ig8);
        iv8.setFitWidth(300);
        iv8.setFitHeight(100);
        iv8.setPreserveRatio(true);
        iv8.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Sanfe Deep Purifying Nose Strips for Women & Men ");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("With Fuji Green Tea & Witch Hazel extracts | Removes Whiteheads | Blackheads \nand cleanses pores (Pack of 6) |Instant Result");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tSanfe\nItem Form:        \t\tPen Product\nBenefits:     \t\t\tpores/blackheads/whiteheads\nScent:  \t\t\t\tGreen Tea \nMaterial Type:\t\t\tFree Chemical Free\nSkin Type:\t\t\tAll \nSpecial Feature:\t\tTravel Size\nIncluded Components:  Skin Treaitment Mask\nNet Quantity:\t\t\t1.00 count\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("blackheads\\blackheads1.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheadsinfo1.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2,click3);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheadsinfo1.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Sanfe Deep Purifying Nose Strips for Women & Men");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹600");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹495");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹600");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹105");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹495");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb8 = new Label("\t  Anti-Acne Gel  \n\t\t 500");
        //lb1.setFont(new Font(20));
        lb8.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l8 = new Label("Bella Vita Organic Anti Acne, Pimple\nRemoval,Face Gel,\nCream for Marks,50gm");
        l8.setFont(new Font(15));

        //backbutton

        Button BackButton= new Button("Back");
        BackButton.setLayoutX(10);
        BackButton.setLayoutY(10);
        BackButton.setOnAction(e -> {
            System.out.println("Button clicked!");
            Stage popupStage = new Stage();
            StackPane popupLayout = new StackPane();
            Label popupLabel = new Label("This is a popup window!");
            popupLayout.getChildren().add(popupLabel);
            Scene popupScene = new Scene(popupLayout, 800, 600);
            popupStage.setScene(popupScene);
            popupStage.show();
        });

        VBox vb1 = new VBox(iv1,lb1,l1);
        vb1.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb1.setPadding(new Insets(10));

        VBox vb2 = new VBox(iv2,lb2,l2);
        vb2.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb2.setPadding(new Insets(10));

        VBox vb3 = new VBox(iv3,lb3,l3);
        vb3.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb3.setPadding(new Insets(10));

        VBox vb4 = new VBox(20,iv4,lb4,l4);
        vb4.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb4.setPadding(new Insets(10));

        VBox vb5 = new VBox(iv5,lb5,l5);
        vb5.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb5.setPadding(new Insets(10));

        VBox vb6 = new VBox(iv6,lb6,l6);
        vb6.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb6.setPadding(new Insets(10));

        VBox vb7 = new VBox(iv7,lb7,l7);
        vb7.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb7.setPadding(new Insets(10));

        VBox vb8 = new VBox(15,iv8,lb8,l8);
        vb8.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb8.setPadding(new Insets(10));

        HBox hb1=new HBox(50,vb1,vb2,vb3,vb4);
        hb1.setLayoutX(200);
        hb1.setLayoutY(100);

        HBox hb2=new HBox(50,vb5,vb6,vb7,vb8);
        hb2.setLayoutX(200);
        hb2.setLayoutY(400);

        Group gr = new Group(BackButton,label1,hb1,hb2);
        Scene  sc = new Scene(gr);
        prStage.setScene(sc);
        sc.setFill(Color.BISQUE);
        prStage.show();
    }
}