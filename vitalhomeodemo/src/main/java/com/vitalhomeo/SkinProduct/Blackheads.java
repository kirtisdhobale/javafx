package com.vitalhomeo.SkinProduct;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;


public class Blackheads extends Application {

    @Override
    public void start(Stage prStage) throws Exception {
    
        prStage.setTitle("VitalHomeo");
        prStage.setHeight(700);
        prStage.setWidth(800); 
        prStage.centerOnScreen();

        Label label1 = new Label("Recommonded For BlackHeads..");
        label1.setBackground(new Background(new BackgroundFill(Color.CADETBLUE, CornerRadii.EMPTY, null)));
        label1.setPadding(new javafx.geometry.Insets(10));
        label1.setStyle("-fx-font-size:25;-fx-font-weight:bold");
        label1.setLayoutX(600);
        label1.setLayoutY(20);
        

        Image ig1=new Image("resources\\blackheads\\blackheads1.png");
        ImageView iv1 =new ImageView(ig1);
        iv1.setFitWidth(500);
        iv1.setFitHeight(150);
        iv1.setPreserveRatio(true);
        iv1.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Sanfe Deep Purifying Nose Strips for Women & Men ");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("With Fuji Green Tea & Witch Hazel extracts | Removes Whiteheads | Blackheads \nand cleanses pores (Pack of 6) |Instant Result");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tSanfe\nItem Form:        \t\tPen Product\nBenefits:     \t\t\tpores/blackheads/whiteheads\nScent:  \t\t\t\tGreen Tea \nMaterial Type:\t\t\tFree Chemical Free\nSkin Type:\t\t\tAll \nSpecial Feature:\t\tTravel Size\nIncluded Components:  Skin Treaitment Mask\nNet Quantity:\t\t\t1.00 count\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("blackheads\\blackheads1.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheadsinfo1.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button backButton= new Button("Back");
               backButton.setLayoutX(10);
               backButton.setLayoutY(10);
               backButton.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox,backButton);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheadsinfo1.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Sanfe Deep Purifying Nose Strips for Women & Men");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹600");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹495");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹600");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹105");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹495");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                Button backButton= new Button("Back");
                backButton.setLayoutX(10);
                backButton.setLayoutY(10);
                backButton.setOnAction(e -> {
                     cartStage.close();
                });

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox,backButton);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb1 = new Label("Sanfe Deep Purifying Nose\nStrips for Women & Men");
        //lb1.setFont(new Font(20));
        lb1.setStyle("-fx-font-size:17;-fx-font-weight:bold");
        lb1.setAlignment(Pos.CENTER);

        Label l1 = new Label("With Fuji Green Tea & \nWitch Hazel extracts ");
        l1.setFont(new Font(15));
        
        //image2

        Image ig2=new Image("blackheads\\blackheads2.png");
        ImageView iv2 =new ImageView(ig2);
        iv2.setFitWidth(800);
        iv2.setFitHeight(200);
        iv2.setPreserveRatio(true);
        iv2.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Lauda Blackhead Dissolving Gel - Powerful Exfoliant - Blackhead Remover & Acne Treatment, 30ml");
               popupLabel.setStyle("-fx-font-size:20;-fx-font-weight:bold");

               Label popuplabel2=new Label("DISSOLVES BLACKHEADS & UNCLOGS PORES: The oil free blackhead remover gel with bioactive ingredients helps to deep \ncleanse pores, control oil production and remove impurities.\n Very powerful blackhead treatment for Oily Skin and Acne Prone Skin. \nNot Suitable for Sensitive Skin."+
               "\n\nIngredents:OIL CONTROL & CLEAR COMPLEXION: Lactic Acid and combined with natural ingredients Nettle, Hops and \nWitch-hazel Extract helps to unclog pores and dissolve blackheads. \nFennel Extact, Malic Acid, Melissa Officinalis and Aspartic Acid help to normalize oil production \nso that healthy oxygen can reach the skin.\n QUALITY YOU CAN TRUST - This formula is used by Dermatologists, Spas and Estheticians alike, \nwho use this effective formula for their clients with extraordinary results. \nConcept Skin products are made in an FDA registered Facility in the USA using only the finest natural and organic \ningredients.");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popuplabel2);

               Image ig1=new Image("blackheads\\blackheads2.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(1000);
               iv1.setFitHeight(450);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });


               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,hBox);
               vBox.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vb);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 800);
               popupStage.setScene(popupScene);
               popupStage.show();
            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }  
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheads2.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Lauda Blackhead Dissolving Gel - Powerful Exfoliant -\nBlackhead Remover & Acne Treatment, 30ml");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹600");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹495");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹600");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹105");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹495");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb2 = new Label("    Lauda Blackhead Gel  \n\t\t3,036");
        //lb2.setFont(new Font(20));
        lb2.setStyle("-fx-font-size:17;-fx-font-weight:bold");
        Label l2 = new Label("   Helps To Remove Acne");
        l2.setFont(new Font(15));

        Image ig3=new Image("blackheads/blackheadsmask3.png");
        ImageView iv3 =new ImageView(ig3);
        iv3.setFitWidth(500);
        iv3.setFitHeight(150);
        iv3.setPreserveRatio(true);
        iv3.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("UrbanGabru Charcoal Peel Off Mask for Men & Women");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("UrbanGabru Charcoal Peel Off Mask for Men & Women |\\n" + //
                                      " Removes Blackheads and Whiteheads |\\n" + //
                                      " Active Cooling Effect |\\n" + //
                                      " Deep Skin Purifying Cleansing (60 gm)");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tADJD\nModel Name:        \t\tBlackhead Whitehead Remover Charcoal Peel Off Black Mask For Pore Acne\nOrganic:     \t\t\tNo\nIdeal For:  \t\t\t\tmen & Women\nSkin Type:\t\t\tAll \nNet Quantity:\t\t\t130 ml\nNumber of Items\t\t1");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("blackheads\\blackheads1.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheadsmask3.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,click3);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheadsmask3.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("UrbanGabru Charcoal Peel Off Mask");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹600");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹495");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹499");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹105");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹279");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb3 = new Label("   UrbanGabru Charcoal Peel \n\t\tOff Mask  \n\t\t  500");
        lb3.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l3 = new Label("    Removes Blackheads and \n\t\tWhiteheads ");
        l3.setFont(new Font(15));

        Image ig4=new Image("blackheads\\blackheads4.png");
        ImageView iv4 =new ImageView(ig4);
        iv4.setFitWidth(600);
        iv4.setFitHeight(150);
        iv4.setPreserveRatio(true);
        iv4.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("4rever Blackhead & Whitehead Remover 8g");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("1] This breakthrough formula helps perfect removal of blackheads & whiteheads and unclogged pores. \r\n" + //
                                      "2] It exfoliates away dead skin cells, deep-seated grime, toxins and excess oil. \r\n" + //
                                      "3]It steadily improves skin health and keeps your complexion smooth & supple.\r\n" + //
                                      "4]One pack - 8g\r\n" + //
                                      "");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("This breakthrough formula helps perfect removal of blackheads & whiteheads and unclogged pores. \nIt exfoliates away dead skin cells, deep seated grime, toxins and excess oil. It steadily improves skin health and keeps your complexion smooth & supple.\r\n" + //
                                      "\r\n" + //
                                      "How to Use:\r\n" + //
                                      "Apply the product on your cleansed skin and cover with a folded facial tissue. \nLeave it for 10 min to dry and peel off smoothly in upwards direction.");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("blackheads\\blackheads4-2.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheads4.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2,click3);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheads4.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Blackhead & Whitehead Remover 30g");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹6000");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹4095");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹6000");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹1005");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹4095");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb4 = new Label("Blackhead & Whitehead \n\tRemover");
        //lb1.setFont(new Font(20));
        lb4.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l4 = new Label("helps perfect removal of blackheads \n& whiteheads and unclogged pores.");
        l4.setFont(new Font(15));

        Image ig5=new Image("blackheads\\blackheads5.png");
        ImageView iv5 =new ImageView(ig5);
        iv5.setFitWidth(500);
        iv5.setFitHeight(150);
        iv5.setPreserveRatio(true);
        iv5.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("POP MODERN.C Blackhead Remover Mask with Tea Tree Cream \nCharcoal Peel Off Facial Mask Blackheads Whiteheads Removal Deep Cleansing \nPore Purifying Acne Face Mask with Brush Pimple Extractor 4-in-1 Kit");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("DEEP CLEANSING AND BLACKHEAD REMOVER: \nThis blackhead removal mask kit contains activated bamboo charcoal extracts can effectively \nclean the deep dirt of the skin. Our Black facial mask with very strong absorptivity,\nit helps remove blackheads, whiteheads and impurities. Make you skin looks cleaner and tightening.");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Brand:        \t\t\tPopMorden\nItem Form:        \t\tCream\nColor:     \t\t\t\tblack\nSkin Type:\t\t\tAcne Prone\nNumber of Items\t\t4");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("blackheads\\blackheads5-2.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheads5.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,click3);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheads5.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("POP MODERN.C Blackheads t5Remover Mask");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹7500");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹5000");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("32% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹7500");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹1500");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹5000");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb5 = new Label("POP MODERN.C Blackhead \n\tRemover Mask");
        //lb1.setFont(new Font(20));
        lb5.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l5 = new Label("Tea Tree Cream Charcoal Peel\n\tOff Facial Mask");
        l5.setFont(new Font(15));

        Image ig6=new Image("blackheads\\blackheads6.png");
        ImageView iv6 =new ImageView(ig6);
        iv6.setFitWidth(500);
        iv6.setFitHeight(150);
        iv6.setPreserveRatio(true);
        iv6.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Some By Mi Bye Bye Blackhead");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("30 Days Miracle Green Tea Tox Bubble Cleanser");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Some By Mi Bye Bye Blackhead | Best Face Cleanser in Pakistan The fine BHA foam appears by itself after application and melts\n away blackheads. The fresh cream foam, containing live green tea leaves and konjac granules, removes any impurities left remaining in the pores.\nThis cleanser helps clean out pores with 16 teas and natural BHA bubbles. This works to provide intensive blackhead care, \npore cleansing for clearer skin and immediately brightens skin just by washing your face. The fine BHA foam appears by itself after application \nand melts away blackheads. The fresh cream foam which contains live green tea leaves and konjac granules helps remove any impurities in the pores. \nFEATURES Helps minimizes pores. Protects against the occurrence of blackheads and whiteheads. \nThe steps for a magical 5 minutes to cleanse your pores:");
               popuplabel2.setStyle("-fx-font-size:18");

               Label popuplabel3 = new Label("How to use:\r\n" + //
                                      "\r\n" + //
                                      "5 minutes intensive blackhead care:\r\n" + //
                                      "\r\n" + //
                                      "1. Apply a thin layer evenly on the dry skin, leave on 3-5 minutes and let the bubble formed.\r\n" + //
                                      "\r\n" + //
                                      "2. Rub slightly with a small amount of water in circular motion to cleanse the skin impurities.\r\n" + //
                                      "\r\n" + //
                                      "3. Rinse off with lukewarm water thoroughly.\r\n" + //
                                      "\r\n" + //
                                      "Daily cleansing:\r\n" + //
                                      "\r\n" + //
                                      "1. Apply a thin layer on the entire dry face evenly and wait for the bubble formed.\n2. Rub slightly with a small amount of water in a circular motion for cleansing.\n3. Rinse off with lukewarm water thoroughly.");
               popuplabel3.setStyle("-fx-font-size:18;");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2,popuplabel3);

               Image ig2=new Image("blackheads\\blackheads1.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheads6.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,hBox,click3);
               vBox.setLayoutX(80);


               HBox product1= new HBox(50,vBox,vb);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheads6.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Some By Mi Bye Bye Blackhead \n30 Days Miracle Green Tea Tox Bubble Cleanser");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹5700");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹3280");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("42% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹5700");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹2420");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹3280");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb6 = new Label("Bye Bye Blackhead ");
        //lb1.setFont(new Font(20));
        lb6.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l6 = new Label("Green Tea Tox Bubble Cleanser");
        l6.setFont(new Font(15));

        Image ig7=new Image("blackheads\\blackheads7.png");
        ImageView iv7 =new ImageView(ig7);
        iv7.setFitWidth(500);
        iv7.setFitHeight(150);
        iv7.setPreserveRatio(true);
        iv7.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Some By Mi Bye Bye Blackhead");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("30 Days Miracle Green Tea Tox Bubble Cleanser");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("");
               popuplabel2.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2);

               Image ig2=new Image("blackheads\\blackheads1.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(700);
               iv2.setFitHeight(300);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheads7.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,click3);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheads7.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Sanfe Deep Purifying Nose Strips for Women & Men");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹600");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹495");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("17% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹600");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹105");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹495");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb7 = new Label("DERMATOUCH Bye Acne\n\t\t 500");
        //lb1.setFont(new Font(20));
        lb7.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l7 = new Label("DERMATOUCH Bye Bye Acne\nScars & Marks Cream Formulated\nSpecially");
        l7.setFont(new Font(15));

        Image ig8=new Image("blackheads\\blackheads8.png");
        ImageView iv8 =new ImageView(ig8);
        iv8.setFitWidth(300);
        iv8.setFitHeight(100);
        iv8.setPreserveRatio(true);
        iv8.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               System.out.println("Image Clicked");
               Stage popupStage = new Stage();
               StackPane popupLayout = new StackPane();
               Label popupLabel = new Label("Blackheads and Whiteheads Removal");
               popupLabel.setStyle("-fx-font-size:25;-fx-font-weight:bold");

               Label popupLabel3 = new Label("A Natural and Chemical Free Scrub\r\n" + //
                                      "A super effective scrub with the goodness of Nagkesar and Vetiver!");
               popupLabel3.setStyle("-fx-font-size:18;");

               Label popuplabel2=new Label("Blackheads and Whiteheads Removal Scrub Benefits:");
               popuplabel2.setStyle("-fx-font-size:18;-fx-font-weight:bold");

               Label popuplabel3=new Label("1. Helps in easy extraction of blackheads and whiteheads\r\n" + //
                                      "\r\n" + //
                                      "2. Prevents the formation of new ones\r\n" + //
                                      "\r" + //
                                      "3. Shrinks open pores\r\n" + //
                                      "\r\n" + //
                                      "4. Unclogs pores\r\n" + //
                                      "\r\n" + //
                                      "5. Balances oil in the skin\r\n" + //
                                      "\r\n" + //
                                      "6. Extracts excessive oil and debris\r\n" + //
                                      "\r\n" + //
                                      "7. Removes dead skin cells");
               popuplabel3.setStyle("-fx-font-size:18");

               VBox vb=new VBox(15,popupLabel,popupLabel3,popuplabel2,popuplabel3);

               Image ig2=new Image("blackheads\\blackheads8.png");
               ImageView iv2 =new ImageView(ig2);
               iv2.setFitWidth(500);
               iv2.setFitHeight(200);
               iv2.setPreserveRatio(true);
               iv2.setLayoutX(250);
               iv2.setLayoutY(400);

               Image ig1=new Image("blackheads\\blackheads8-2.png");
               ImageView iv1 =new ImageView(ig1);
               iv1.setFitWidth(700);
               iv1.setFitHeight(300);
               iv1.setPreserveRatio(true);
               iv1.setLayoutX(250);
               iv1.setLayoutY(400);

               Button buyNowButton = createButton("Buy Now", "\uD83D\uDCB5",null, "#4CAF50"); // 💵 symbol with green background
               buyNowButton.setMaxSize(180, 40);     

               Button addToCartButton = createButton("Add to Cart", "\uD83D\uDED2",null,"#FFC107"); // 🛒 symbol with amber background
               addToCartButton.setMaxSize(180, 40);
               addToCartButton.setOnAction(e -> {
                showAddedToCartPage(popupStage);
               });



               Button click3= new Button("Back");
               click3.setLayoutX(10);
               click3.setLayoutY(10);
               click3.setOnAction(e -> {
                    popupStage.close();
               });

               HBox hBox = new HBox(15,addToCartButton,buyNowButton);
               VBox vBox = new VBox(15,iv1,iv2);
               vBox.setLayoutX(80);

               VBox vBox1= new VBox(15,vb,hBox,click3);
               vBox1.setLayoutX(80);

               HBox product1= new HBox(50,vBox,vBox1);
               product1.setLayoutX(200);
               product1.setLayoutY(400);
       

               popupLayout.getChildren().add(product1);
               Scene popupScene = new Scene(popupLayout, 2000, 1000);
               popupStage.setScene(popupScene);
               popupStage.show();


            }   

            private Button createButton(String text, String symbol, String string,String backgroundColor) {
                Button button = new Button(text + "  " + symbol);
                button.setStyle("-fx-font-size: 16pt; -fx-background-color: " + backgroundColor + ";");
                return button;
            }   
            private void showAddedToCartPage(Stage parentStage) {
                Stage cartStage = new Stage();
                HBox mainLayout = new HBox(20);
                mainLayout.setPadding(new Insets(20));

                // Left side: Product Details
                VBox productDetails = new VBox(10);
                productDetails.setPadding(new Insets(10));
                productDetails.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                // Product image
                ImageView productImage = new ImageView(new Image("blackheads\\blackheads8.png"));
                productImage.setFitWidth(100);
                productImage.setFitHeight(100);

                // Product name
                Label productName = new Label("Blackheads and Whiteheads Removal - \nA Natural and Chemical Free Scrub");
                productName.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");


                // Delivery details
                Label deliveryDetails = new Label("Delivery by Mon Jul 15  |  Free");
                deliveryDetails.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                // Price and discount
                HBox priceDetails = new HBox(10);
                Label originalPrice = new Label("₹450");
                originalPrice.setStyle("-fx-font-size: 14px; -fx-text-decoration: line-through;");
                Label discountedPrice = new Label("₹399");
                discountedPrice.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: red;");
                Label discount = new Label("7% Off");
                discount.setStyle("-fx-font-size: 14px; -fx-text-fill: green;");

                priceDetails.getChildren().addAll(originalPrice, discountedPrice, discount);

                // Minimum order quantity
                Label minOrderQty = new Label("Minimum Order Quantity: 5");
                minOrderQty.setStyle("-fx-font-size: 14px; -fx-background-color: #fff3cd; -fx-padding: 5px;");

                productDetails.getChildren().addAll(productImage, productName, deliveryDetails, priceDetails, minOrderQty);

                // Right side: Price Details
                VBox priceDetailsBox = new VBox(10);
                priceDetailsBox.setPadding(new Insets(10));
                priceDetailsBox.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px;");

                Label priceDetailsLabel = new Label("PRICE DETAILS");
                priceDetailsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                GridPane priceGrid = new GridPane();
                priceGrid.setVgap(10);
                priceGrid.setHgap(20);

                Label priceLabel = new Label("Price (1 items)");
                Label priceAmount = new Label("₹450");

                Label discountLabel = new Label("Discount");
                Label discountAmount = new Label("– ₹51");
                discountAmount.setStyle("-fx-text-fill: green;");

                Label deliveryChargesLabel = new Label("Delivery Charges");
                Label deliveryChargesAmount = new Label("Free");
                deliveryChargesAmount.setStyle("-fx-text-fill: green;");

                Label totalAmountLabel = new Label("Total Amount");
                Label totalAmount = new Label("₹399");
                totalAmount.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

                priceGrid.add(priceLabel, 0, 0);
                priceGrid.add(priceAmount, 1, 0);
                priceGrid.add(discountLabel, 0, 1);
                priceGrid.add(discountAmount, 1, 1);
                priceGrid.add(deliveryChargesLabel, 0, 2);
                priceGrid.add(deliveryChargesAmount, 1, 2);
                priceGrid.add(totalAmountLabel, 0, 3);
                priceGrid.add(totalAmount, 1, 3);

                priceDetailsBox.getChildren().addAll(priceDetailsLabel, priceGrid);
                mainLayout.getChildren().addAll(productDetails, priceDetailsBox);


                Scene cartScene = new Scene(mainLayout, 800, 600);
                cartStage.setScene(cartScene);
                cartStage.show();
            }
        });

        Label lb8 = new Label("Blackheads and Whiteheads \n\tRemoval");
        //lb1.setFont(new Font(20));
        lb8.setStyle("-fx-font-size:17;-fx-font-weight:bold");

        Label l8 = new Label("A Natural and Chemical Free Scrub");
        l8.setFont(new Font(15));

        //BACKBUTTON

        Button BackButton= new Button("Back");
        BackButton.setLayoutX(10);
        BackButton.setLayoutY(10);
        BackButton.setOnAction(e -> {
            System.out.println("Button clicked!");
        });

        VBox vb1 = new VBox(iv1,lb1,l1);
        vb1.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb1.setPadding(new Insets(10));

        VBox vb2 = new VBox(iv2,lb2,l2);
        vb2.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb2.setPadding(new Insets(10));

        VBox vb3 = new VBox(iv3,lb3,l3);
        vb3.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb3.setPadding(new Insets(10));

        VBox vb4 = new VBox(20,iv4,lb4,l4);
        vb4.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb4.setPadding(new Insets(10));

        VBox vb5 = new VBox(iv5,lb5,l5);
        vb5.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb5.setPadding(new Insets(10));

        VBox vb6 = new VBox(iv6,lb6,l6);
        vb6.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb6.setPadding(new Insets(10));

        VBox vb7 = new VBox(iv7,lb7,l7);
        vb7.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb7.setPadding(new Insets(10));

        VBox vb8 = new VBox(15,iv8,lb8,l8);
        vb8.setStyle( "-fx-background-color:white;-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.8), 10, 0.5, 0, 0);-fx-cursor: hand;-fx-border-color: grey;-fx-border-width: 2px;-fx-border-radius: 20px;-fx-background-radius: 20px");
        vb8.setPadding(new Insets(10));

        HBox hb1=new HBox(50,vb1,vb2,vb3,vb4);
        hb1.setLayoutX(170);
        hb1.setLayoutY(90);

        HBox hb2=new HBox(50,vb5,vb6,vb7,vb8);
        hb2.setLayoutX(170);
        hb2.setLayoutY(420);

        Group gr = new Group(BackButton,label1,hb1,hb2);
        Scene  sc = new Scene(gr);
        prStage.setScene(sc);
        sc.setFill(Color.BISQUE);
        prStage.show();
    }
}