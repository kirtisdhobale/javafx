package com.vitalhomeo.Controller;

import com.vitalhomeo.Dashboards.AdminPage;
import com.vitalhomeo.Dashboards.Userpage;
import com.vitalhomeo.Controller.LoginController;
import com.vitalhomeo.FirebaseConfig.DataService;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.concurrent.ExecutionException;

public class LoginController {

    private static Stage primaryStage;
    private Scene loginScene;
    private Scene userScene;
    private static Scene adminScene;
    private DataService dataService;
    public static String key;

    public LoginController(Stage primaryStage) {
        this.primaryStage = primaryStage;
        dataService = new DataService();
        initScenes();
    }

    public void setPrimaryStage(Scene scene) {
        if (primaryStage != null) {
            primaryStage.setScene(scene);
        } else {
            System.err.println("Primary stage is not set. Cannot set scene.");
        }
    }

    private void initScenes() {
        initLoginScene();
    }

    private void initLoginScene() {
        Label userLabel = new Label("Username");
        TextField userTextField = new TextField();
        Label passLabel = new Label("Password");
        PasswordField passField = new PasswordField();

        Button adminLoginButton = new Button("Admin Login");
        Button userLoginButton = new Button("User Login");
        Button loginButton = new Button("Login");
        Button signupButton = new Button("Signup");

        loginButton.setOnAction(event -> {
            handleLogin(userTextField.getText(), passField.getText());
            userTextField.clear();
            passField.clear();
        });

        signupButton.setOnAction(event -> {
            showSignupScene();
            userTextField.clear();
            passField.clear();
        });

        adminLoginButton.setOnAction(event -> {
            handleAdminLogin(userTextField.getText(), passField.getText());
            userTextField.clear();
            passField.clear();
        });

        userLoginButton.setOnAction(event -> {
            handleUserLogin(userTextField.getText(), passField.getText());
            userTextField.clear();
            passField.clear();
        });

        userLabel.setStyle("-fx-text-fill: white;-fx-font-weight:bold;-fx-font-size:12;-fx-max-width:200;-fx-max-height:50;");
        passLabel.setStyle("-fx-text-fill: white;-fx-font-weight:bold;-fx-font-size:12;-fx-max-width:200;-fx-max-height:50;");

        VBox fieldBox1 = new VBox(10, userLabel, userTextField);
        fieldBox1.setMaxSize(300, 30);
        VBox fieldBox2 = new VBox(10, passLabel, passField);
        fieldBox2.setMaxSize(300, 30);
        HBox buttonBox = new HBox(50, adminLoginButton, userLoginButton, signupButton);
        buttonBox.setMaxSize(350, 30);
        buttonBox.setAlignment(Pos.CENTER);

        userTextField.setStyle("-fx-set-pref-width:350");
        passField.setStyle("-fx-set-pref-width:350");

        VBox vbox = new VBox(20, fieldBox1, fieldBox2, buttonBox);
        vbox.setStyle("-fx-background-image:url('https://img.freepik.com/free-photo/digital-world-banner-background-remixed-from-public-domain-by-nasa_53876-124622.jpg?semt=sph')");
        vbox.setAlignment(Pos.CENTER);

        loginScene = new Scene(vbox, 700, 700);
    }

    private void initUserScene() {
        Userpage userPage = new Userpage(dataService);
        userScene = new Scene(userPage.createUserScene(this::handleLogout), 700, 700);
    }

    private void initAdminScene() {
        AdminPage adminPage = new AdminPage(this, dataService);
        adminScene = new Scene(adminPage.createAdminDashboard(this::handleLogout), 700, 700);
    }

    public Scene getLoginScene() {
        return loginScene;
    }

    public void showLoginScene() {
        primaryStage.setScene(loginScene);
        primaryStage.setTitle("Login");
        primaryStage.show();
    }

    private void handleUserLogin(String username, String password) {
        try {
            if (dataService.authenticateUser(username, password) && !dataService.isAdmin(username)) {
                key = username;
                initUserScene();
                primaryStage.setScene(userScene);
                primaryStage.setTitle("User Dashboard");
            } else {
                System.out.println("Invalid client credentials");
            }
        } catch (ExecutionException | InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void handleLogin(String username, String password) {
        try {
            if (dataService.authenticateUser(username, password)) {
                key = username;
                initUserScene();
                primaryStage.setScene(userScene);
                primaryStage.setTitle("User Dashboard");
            } else {
                System.out.println("Invalid credentials");
                key = null;
            }
        } catch (ExecutionException | InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void handleAdminLogin(String username, String password) {
        try {
            if (dataService.authenticateUser(username, password) && dataService.isAdmin(username)) {
                initAdminScene();
                primaryStage.setScene(adminScene);
                primaryStage.setTitle("Admin Dashboard");
            } else {
                System.out.println("Invalid admin credentials");
            }
        } catch (ExecutionException | InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void showSignupScene() {
        SignUpController signupController = new SignUpController(this);
        Scene signupScene = signupController.createSignupScene(primaryStage);
        primaryStage.setScene(signupScene);
        primaryStage.setTitle("Signup");
        primaryStage.show();
    }

    private void handleLogout() {
        primaryStage.setScene(loginScene);
        primaryStage.setTitle("Login");
    }

    public static void returnToAdminView() {
        primaryStage.setScene(adminScene);
        primaryStage.setTitle("Admin Dashboard");
    }
}

