package com.vitalhomeo.Dashboards;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.vitalhomeo.FirebaseConfig.DataService;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Skin;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Credintials extends Application{
    public Credintials() {
        // Initialization code, if needed
    }

    public static void main(String[] args){
        System.out.println("Hello");
        Application.launch(args);
    }
    private DataService dataService; // DataService for Firestore operations
    private Label dataMsg; // Label to display status messages

    public Credintials(DataService dataService) {
        this.dataService = dataService;
    }

    // Method to create and return the user interface VBox for project data entry
    public VBox createUserScene(Runnable logoutHandler) {
        // Initialize dataMsg for displaying status messages
        dataMsg = new Label();
        return null;
    }


    @Override
    public void start(Stage prStage) throws Exception {
        prStage.setTitle("Credentials Page");
        prStage.setHeight(600);
        prStage.setWidth(800);

        Label lb = new Label("Welcome To VitalHomeo..");
        lb.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        lb.setLayoutX(150);
        lb.setLayoutY(80);

        Label nameLabel = new Label("Enter Your Name:");
        nameLabel.setStyle("-fx-font-size:12;-fx-font-weight:bold");

        TextField Name = new TextField();
        Name.setPromptText("Name");

        VBox groupBox = new VBox(10, nameLabel,Name);
        groupBox.setMaxSize(300, 20);

        Label mobileNumber = new Label("Enter mobile number:");
        mobileNumber.setStyle("-fx-font-size:12;-fx-font-weight:bold");

        TextField mobTextField = new TextField();
        mobTextField.setPromptText("Your Contact");

        VBox mobBox = new VBox(10, mobileNumber, mobTextField);
        mobBox.setMaxSize(300, 20);

        Label AgeLabel = new Label("Enter Your Age:");
        AgeLabel.setStyle("-fx-font-size:12;-fx-font-weight:bold");

        TextField Age = new TextField();
        Age.setPromptText("Your Age");

        VBox projectBox = new VBox(10,AgeLabel, Age);
        projectBox.setMaxSize(300, 20);

        Label SkinLabel = new Label("Enter Your Skin Type:");
        SkinLabel.setStyle("-fx-font-size:12;-fx-font-weight:bold");

        TextField SkinType = new TextField();
        SkinType.setPromptText("Dry,Oily etc");

        VBox SkinBox = new VBox(10,SkinLabel,SkinType);
        SkinBox.setMaxSize(300, 20);

        Button addButton = new Button("Add Data");
        addButton.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        HBox buttonBox = new HBox(addButton);
        buttonBox.setAlignment(Pos.CENTER);

        Button BackButton = new Button("Back");

         addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Create a map with project data
                Map<String, Object> data = new HashMap<>();
                data.put("groupName", Name.getText());
                data.put("projectName",mobTextField.getText());
                data.put("leaderName", Age.getText());
                data.put("mobileNum", mobTextField.getText());
                data.put("member2", SkinType.getText());

                try {
                    // Attempt to add data to Firestore
                    DataService.addData("collectionName", Age.getText(), data);
                    dataMsg.setText("Added successfully"); // Update status message
                    // Clear input fields after successful addition
                    Name.clear();
                    mobTextField.clear();
                    Age.clear();
                    SkinType.clear();
                } catch (ExecutionException | InterruptedException ex) {
                    dataMsg.setText("Something went wrong"); // Update status message
                    ex.printStackTrace();
                }
            }
        });


        VBox vButton = new VBox(BackButton);
        BackButton.setAlignment(Pos.TOP_LEFT);


        VBox vButton2 = new VBox(15,nameLabel,Name,mobileNumber,mobTextField,AgeLabel,Age,SkinLabel,SkinType,addButton);
        vButton2.setAlignment(Pos.CENTER);
        vButton2.setLayoutX(300);
        vButton2.setLayoutY(130);
        vButton2.setStyle("-fx-background-image:url('file:https://www.google.com/url?sa=i&url=https%3A%2F%2Fstock.adobe.com%2Fsearch%3Fk%3Dhomeopathy%2Bbackground&psig=AOvVaw313X4lksvus3D95YGbZ0NF&ust=1720158961116000&source=images&cd=vfe&opi=89978449&ved=0CBEQjRxqFwoTCOiE7vPZjIcDFQAAAAAdAAAAABAE')");

        Group gr= new Group(vButton,lb,vButton2);
        Scene sc=new Scene(gr,200,300);
        sc.setFill(Color.AQUA);
        prStage.setScene(sc);
        prStage.show();

    }
}



    

